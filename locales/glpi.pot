# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-01-09 16:46+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: hook.php:48 setup.php:82 inc/common.class.php:123
#: inc/preference.class.php:182 inc/profile.class.php:43
#: inc/profile.class.php:71 inc/profile.class.php:75 inc/profile.class.php:93
msgid "Print to pdf"
msgstr ""

#: inc/changecost.class.php:58
msgid "No cost for this change"
msgstr ""

#: inc/change_item.class.php:185 inc/change_problem.class.php:242
msgid "No associated change"
msgstr ""

#: inc/change_problem.class.php:69 inc/item_problem.class.php:64
#: inc/item_problem.class.php:181
msgid "No associated problem"
msgstr ""

#: inc/changevalidation.class.php:49
msgid "Approvals for the change"
msgstr ""

#: inc/common.class.php:304
msgid "No note found"
msgstr ""

#: inc/computerantivirus.class.php:72
msgid "No Antivirus"
msgstr ""

#: inc/computerdisk.class.php:84
msgid "No associated volume"
msgstr ""

#: inc/computer_item.class.php:125
msgid "No printer"
msgstr ""

#: inc/computer_item.class.php:129
msgid "No monitor"
msgstr ""

#: inc/computer_item.class.php:133
msgid "No peripheral"
msgstr ""

#: inc/computer_item.class.php:137
msgid "No phone"
msgstr ""

#: inc/computer_softwareversion.class.php:291
msgid "No installed software"
msgstr ""

#: inc/document.class.php:67
msgid "No associated documents"
msgstr ""

#: inc/document.class.php:69
msgid "Associated documents"
msgstr ""

#: inc/infocom.class.php:182
msgid "No financial information"
msgstr ""

#: inc/item_ticket.class.php:210
msgid "No associated ticket"
msgstr ""

#: inc/knowbaseitem.class.php:85
msgid "No question found"
msgstr ""

#: inc/preference.class.php:118
msgid "Choose the tables to print in pdf"
msgstr ""

#: inc/preference.class.php:161
msgid "Portrait"
msgstr ""

#: inc/preference.class.php:162
msgid "Landscape"
msgstr ""

#: inc/preference.class.php:168
msgctxt "button"
msgid "Print"
msgstr ""

#: inc/problem_ticket.class.php:243
msgid "No ticket found."
msgstr ""

#: inc/reservation.class.php:73
msgid "No current and future reservations"
msgstr ""

#: inc/reservation.class.php:107
msgid "No past reservations"
msgstr ""

#: inc/reservation.class.php:127
msgid "Item not reservable"
msgstr ""

#: inc/ticket.class.php:458
msgid "Requester information"
msgstr ""

#: inc/ticketcost.class.php:58
msgid "No cost for this ticket"
msgstr ""

#: inc/ticketfollowup.class.php:70
msgid "No followup for this ticket."
msgstr ""

#: inc/ticketfollowup.class.php:77
msgid "Source of followup"
msgstr ""

#: inc/ticketsatisfaction.class.php:61
msgid "1 star"
msgstr ""

#: inc/ticketsatisfaction.class.php:62
msgid "2 stars"
msgstr ""

#: inc/ticketsatisfaction.class.php:63
msgid "3 stars"
msgstr ""

#: inc/ticketsatisfaction.class.php:64
msgid "4 stars"
msgstr ""

#: inc/ticketsatisfaction.class.php:65
msgid "5 stars"
msgstr ""

#: inc/ticketsatisfaction.class.php:81
msgid "No answer"
msgstr ""

#: inc/tickettask.class.php:93
msgid "By user"
msgstr ""

#: inc/tickettask.class.php:97
msgid "By group"
msgstr ""

#: inc/ticketvalidation.class.php:49
msgid "Approvals for the ticket"
msgstr ""
