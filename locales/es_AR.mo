��    !      $  /   ,      �     �     �     �                     )     B  !   W     y  	   �  	   �     �     �     �     �     �          0  
   F     Q     _     m  
   v     �     �     �     �     �     �     �     �  �  �  
   �     �     �     �     �          '     F  "   [     ~     �     �     �  
   �     �     �       #   $     H     `     l     u     �     �     �     �     �     �     �     �     �     	                                                                
                   	                                     !                                       1 star 2 stars 3 stars 4 stars 5 stars Approvals for the change Approvals for the ticket Associated documents Choose the tables to print in pdf Item not reservable Landscape No answer No associated documents No change found. No cost for this change No cost for this ticket No financial information No followup for this ticket. No installed software No monitor No note found No peripheral No phone No printer No problem found. No question found No ticket found. No volume found Portrait Print to pdf Source of followup buttonPrint Project-Id-Version: GLPI Project - Plugin PDF
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-25 20:07+0100
PO-Revision-Date: 2016-02-25 21:03+0000
Last-Translator: Luis Angel Uriarte <luisuriarte@gmail.com>
Language-Team: Spanish (Argentina) (http://www.transifex.com/yllen/glpi-project-plugin-pdf/language/es_AR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_AR
Plural-Forms: nplurals=2; plural=(n != 1);
 1 estrella 2 estrellas 3 estrellas 4 estrellas 5 estrellas Aprobaciones para el cambio Aprobaciones para el incidente Documentos asociados Elegir tablas para imprimir en pdf Elemento no reservable Paisaje Sin respuesta Sin documentos asociados Sin cambio Sin costo para este cambio Sin costo para este incidente Sin información financiera Sin seguimiento para este incidente Sin programas instalado Sin monitor Sin nota Sin periféricos Sin teléfono Sin impresora Sin problema Sin pregunta Sin incidencia Sin volúmen Retrato Imprimir en pdf Fuente del seguimiento Imprimir 