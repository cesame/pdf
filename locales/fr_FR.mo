��    (      \  5   �      p     q     x     �     �     �     �     �     �     �     �  !   �       	   &     0  	   =     G     \     t     �     �     �     �  "   �                =  
   S     ^     l     �     �  
   �     �     �     �     �     �     �       �    	   �  
   �  
   �  
   �  
   �     �          8     L     Z  *   l     �     �     �     �     �     �     	     	     1	     >	     ^	  &   y	     �	     �	     �	     �	     �	     
     +
     A
     T
     e
     ~
     �
     �
     �
     �
     �
     "          #          %          &                        !   $                              
                  '                                   (                    	           1 star 2 stars 3 stars 4 stars 5 stars Approvals for the change Approvals for the ticket Associated documents By group By user Choose the tables to print in pdf Item not reservable Landscape No Antivirus No answer No associated change No associated documents No associated problem No associated ticket No associated volume No cost for this change No cost for this ticket No current and future reservations No financial information No followup for this ticket. No installed software No monitor No note found No past reservations No peripheral No phone No printer No question found No ticket found. Portrait Print to pdf Requester information Source of followup buttonPrint Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-01-09 16:46+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Nelly Mahu-Lasson <nini.lasson@orange.fr>, 2017
Language-Team: French (France) (https://www.transifex.com/yllen/teams/7201/fr_FR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_FR
Plural-Forms: nplurals=2; plural=(n > 1);
 1 étoile 2 étoiles 3 étoiles 4 étoiles 5 étoiles Validations pour ce changement Validations pour ce tickets Documents associés Par le groupe Par l'utilisateur Choisir les tableaux pour l'impression pdf Eléments non réservable Paysage Aucun antivirus Pas de réponse Aucun changement Aucun document associé Aucun problème associé Aucun ticket associé Aucun volume Pas de coût pour ce changement Aucun coût pour ce ticket Aucune réservation actuelle ou future Pas d'information financière Aucun suivi pour ce ticket Aucun logiciel installé Pas d'écran Aucune note Aucune réservation antérieure Pas de périphérique Pas de téléphone Pas d'imprimante Pas de question trouvée Aucun ticket Portrait Impression pdf Information sur le demandeur Source du suivi Imprimer 